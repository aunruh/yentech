var yentech = (function(){
	var words = ["Secret Weapons Nexus","Virility Meter","Michael Crichton Ghola","Red Mercury","District 9 Prawn","Immersive Theater","Rabbit Hole","Bilderberg","Tupac Spiritualism","Dread Walker","Celebrity Symbiote","Offshore Drilling","Life Benefits","Hairless Clone","eVo","Vin Diesel Essence","Mantis Burst","Proto Accelerator","Chinese Visual Preset Bank","Acropolis Forum","Hybrid Genetics","Daemon AI","Performance Athlete","Locust Blueprints","Zero Angel","Dragoon Sketchbook","Hawking Drive","Stim Sim","Data Miner","Botanic Synthesis","Exo-Skin","Groupthink","Angelina Jolie Nudes","Cloudchaser","Holistic Digi Millennial","Disruptive Tech","Reconnaissance","Hunger Games Concept Art","Johnny Depp","Sniper Poetry","Spawn Hatcher","Drone Refuel Tutorial","Skinwalker","Snail Collagen","Bohemian Life Water","Spyder CPU","9/11 Inside Job","Nippon Torrent Crawler","Hunter Seeker","Fortune Cookie Author","Armored Core","Xenix Blade","Post Coital Neo-Shaman","3D Bono","Kowloon Infinity Pool","Trap-Lore","Reptilian Bloodline","Compatible Host","Sub/Dom","Animal Prosthetics","Forensic Team","Xenomorph","Teen Base Jumper","Tribal Face Tattoo","Aural Kabuki","Dead Male Model","Fuzzy Logic","Infared Canopy","Core Competency","Paradigm Shift"];
	var $words = [];

	var isTouchDevice = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|playbook|silk|BlackBerry|BB10|Windows Phone|Tizen|Bada|webOS|IEMobile|Opera Mini)/);
	if( isTouchDevice == null ){ isTouchDevice = false; }

	var jqueryMap = {
		$yen: null,
		$video: null,
		$gallerywrap: null,
		$wordswrap: null
	};

	var configMap = {
		gallerySize: 75
	}

	var setJqueryMap = function(){
		jqueryMap.$yen = jQuery('#yentech');
		jqueryMap.$video = jQuery('#yentech-wrap video');
		jqueryMap.$gallerywrap = jQuery('.yen-gallery-wrap');
		jqueryMap.$wordswrap = jQuery('#yen-words');
	}

	var setVideoSize = function(){
		var ar = 16/9;
		var winar = window.innerWidth / (window.innerHeight + jQuery('.yen-credits').height());
		if(ar<winar){
			jqueryMap.$video.addClass('w100').removeClass('h100');
		}else{
			jqueryMap.$video.addClass('h100').removeClass('w100');
		}
	};

	var sizeGallery = function(){
		var winar = window.innerWidth/window.innerHeight;
		var galw;
		var galh = 1280;

		if(window.innerWidth > window.innerHeight){
			galw = 1706;
		}else{
			galw = 853;
		}

		if(galw/galh>winar){
			var w = window.innerWidth / 100 * configMap.gallerySize;
			var h = galh/galw*w;
			w = Math.ceil(w);
			h = Math.ceil(h);
			jqueryMap.$gallerywrap.css({width: w+'px', height: h+'px'});
		}else{
			var h = window.innerHeight / 100 * configMap.gallerySize;
			var w = galw/galh*h;
			w = Math.ceil(w);
			h = Math.ceil(h);
			jqueryMap.$gallerywrap.css({width: w+'px', height: h+'px'});
		}
	};

	var initGallery = function(){
		var $gallery = jQuery('.yen-gallery').flickity({
			cellAlign: 'left',
			wrapAround: true,
			setGallerySize: false,
			percentPosition: false,
			pageDots: false,
			prevNextButtons: false,
			accessibility: false,
			draggable: true,
			selectedAttraction: 0.055,
			friction: 0.4
		});

		jQuery('.yen-next').on('click', function(){
			$gallery.flickity('next', true);
		});

		jQuery('.yen-prev').on('click', function(){
			$gallery.flickity('previous', true);
		});
	};

	var bindResize = function(){
		jQuery(window).on('resize orientationchange', function(){
			setSizes();
			setVideoSize();
			sizeGallery();
		});
	};

	var handleTouchDevice = function(){
		if(isTouchDevice){
			jQuery('#yentech-wrap').css('background-image', 'url(img/background.jpg)');
			jQuery('#yentech-wrap video').remove();
			jQuery('.yen-gallery-wrap').addClass('opacity1');
		}
	};

	var initWords = function(){
		for (var i = 0; i < words.length; i++) {
			var wordEl = document.createElement("span");
			var content = document.createTextNode(words[i]); 
			wordEl.appendChild(content);

			$words.push(wordEl);
			jqueryMap.$wordswrap.append(wordEl);
		};
	};

	var initAnimation = function(){
		setInterval(function() {
			// get 10 random word elements and their position
			var tenwords = [];
			for (var i = 0; i < 6; i++) {
				var ix = getRandomInt(0, $words.length);
				if(!jQuery($words[ix]).hasClass('show')){
					var x = $words[ix].getBoundingClientRect().width;
					var targetX = window.innerWidth + x;
					var y = getRandomInt(0, window.innerHeight-30);
					tenwords.push([$words[ix], x, y, targetX]);
				}
			};

			// position 10 words
			for (var i = 0; i < tenwords.length; i++) {
				(function(){
					var word = tenwords[i];
					var $wordEl = jQuery(word[0]);
					var speed = getRandomInt(10000, 20000);

					$wordEl.attr('style', '');

					$wordEl.css({
						'-webkit-transform': 'translate(-'+word[1]+'px, '+word[2]+'px)',
						'transform': 'translate(-'+word[1]+'px, '+word[2]+'px)'
					});

					setTimeout(function() {
						$wordEl.css({
							'-webkit-transition': '-webkit-transform '+speed+'ms linear',
							'transition': 'transform '+speed+'ms linear'
						});
						// show word
						$wordEl.addClass('show');
						// hide word after animation has ended

					}, 100);

					setTimeout(function() {
						$wordEl.css({
							'-webkit-transform': 'translate('+word[3]+'px, '+word[2]+'px)',
							'transform': 'translate('+word[3]+'px, '+word[2]+'px)',
						});
						$wordEl[0].addEventListener("transitionend", function(){
							jQuery(this).removeClass('show');
						}, false);
					}, 200);
				})();
			};
		}, 3000);
	};

	// Returns a random integer between min (included) and max (excluded)
	// Using Math.round() will give you a non-uniform distribution!
	function getRandomInt(min, max) {
	  return Math.floor(Math.random() * (max - min)) + min;
	}

	var bindDownArrowClick = function(){
		jQuery('.yen-arrow-down').on('click', function(){
			jQuery('html, body').animate({ scrollTop: '450px' });
		});
	};

	var setSizes = function(){
		jqueryMap.$yen.css('height', window.innerHeight+'px');
	};

	var initModule = function(){
		setJqueryMap();
		setSizes();
		setVideoSize();
		sizeGallery();
		initGallery();
		bindResize();
		handleTouchDevice();
		initWords();
		initAnimation();
		bindDownArrowClick();
		jQuery('.yen-gallery-wrap').removeClass('hover');
	};

	return {
		initModule : initModule
	}
}());

jQuery(document).ready(function(){
	yentech.initModule();
});